{
  "mujeres": [],
  "hombres": [],
  "padron": [
    {
      "Nombre": "HombreBrasil-1",
      "Deporte": "AF",
      "Genero": 1,
      "Nacionalidad": "Brasil"
    },
    {
      "Nombre": "HombreBrasil-1",
      "Deporte": "F",
      "Genero": 1,
      "Nacionalidad": "Brasil"
    },
    {
      "Nombre": "HombreBrasil-1",
      "Deporte": "F",
      "Genero": 1,
      "Nacionalidad": "Brasil"
    },
    {
      "Nombre": "HombreArgentina-1",
      "Deporte": "F",
      "Genero": 1,
      "Nacionalidad": "Argentina"
    },
    {
      "Nombre": "HombreArgentina-1",
      "Deporte": "F",
      "Genero": 1,
      "Nacionalidad": "Argentina"
    },
    {
      "Nombre": "HombreArgentina-1",
      "Deporte": "F",
      "Genero": 1,
      "Nacionalidad": "Argentina"
    },
    {
      "Nombre": "HombreArgentina-1",
      "Deporte": "F",
      "Genero": 1,
      "Nacionalidad": "Argentina"
    },
    {
      "Nombre": "HombreArgentina-1",
      "Deporte": "F",
      "Genero": 1,
      "Nacionalidad": "Argentina"
    },
    {
      "Nombre": "HombreBrasil-1",
      "Deporte": "F",
      "Genero": 1,
      "Nacionalidad": "Brasil"
    },
    {
      "Nombre": "HombreBrasil-1",
      "Deporte": "FA",
      "Genero": 1,
      "Nacionalidad": "Brasil"
    },
    {
      "Nombre": "HombreBrasil-1",
      "Deporte": "FA",
      "Genero": 1,
      "Nacionalidad": "Brasil"
    },
    {
      "Nombre": "HombreBrasil-1",
      "Deporte": "F",
      "Genero": 1,
      "Nacionalidad": "Brasil"
    },
    {
      "Nombre": "HombreUruguay-1",
      "Deporte": "Futbol",
      "Genero": 1,
      "Nacionalidad": "Uruguay"
    },
    {
      "Nombre": "HombreUruguay-1",
      "Deporte": "Basquet",
      "Genero": 1,
      "Nacionalidad": "Uruguay"
    },
    {
      "Nombre": "HombreUruguay-1",
      "Deporte": "Futbol",
      "Genero": 1,
      "Nacionalidad": "Uruguay"
    },
    {
      "Nombre": "HombreUruguay-1",
      "Deporte": "Basquet",
      "Genero": 1,
      "Nacionalidad": "Uruguay"
    },
    {
      "Nombre": "HombreUruguay-1",
      "Deporte": "Futbol",
      "Genero": 1,
      "Nacionalidad": "Uruguay"
    },
    {
      "Nombre": "HombreUruguay-1",
      "Deporte": "Tenis",
      "Genero": 1,
      "Nacionalidad": "Uruguay"
    },
    {
      "Nombre": "HombreUruguay-1",
      "Deporte": "Futbol",
      "Genero": 1,
      "Nacionalidad": "Uruguay"
    },
    {
      "Nombre": "MuBrasil-1",
      "Deporte": "AF",
      "Genero": 0,
      "Nacionalidad": "Brasil"
    },
    {
      "Nombre": "MuBrasil-1",
      "Deporte": "F",
      "Genero": 0,
      "Nacionalidad": "Brasil"
    },
    {
      "Nombre": "MuBrasil-1",
      "Deporte": "F",
      "Genero": 0,
      "Nacionalidad": "Brasil"
    },
    {
      "Nombre": "MuArgentina-1",
      "Deporte": "F",
      "Genero": 0,
      "Nacionalidad": "Argentina"
    },
    {
      "Nombre": "MuArgentina-1",
      "Deporte": "F",
      "Genero": 0,
      "Nacionalidad": "Argentina"
    },
    {
      "Nombre": "MuArgentina-1",
      "Deporte": "F",
      "Genero": 0,
      "Nacionalidad": "Argentina"
    },
    {
      "Nombre": "MuArgentina-1",
      "Deporte": "F",
      "Genero": 0,
      "Nacionalidad": "Argentina"
    },
    {
      "Nombre": "MuArgentina-1",
      "Deporte": "F",
      "Genero": 0,
      "Nacionalidad": "Argentina"
    },
    {
      "Nombre": "MuBrasil-1",
      "Deporte": "F",
      "Genero": 0,
      "Nacionalidad": "Brasil"
    },
    {
      "Nombre": "MuBrasil-1",
      "Deporte": "FA",
      "Genero": 0,
      "Nacionalidad": "Brasil"
    },
    {
      "Nombre": "MuBrasil-1",
      "Deporte": "FA",
      "Genero": 0,
      "Nacionalidad": "Brasil"
    },
    {
      "Nombre": "MuBrasil-1",
      "Deporte": "F",
      "Genero": 0,
      "Nacionalidad": "Brasil"
    },
    {
      "Nombre": "MuUruguay-1",
      "Deporte": "Futbol",
      "Genero": 0,
      "Nacionalidad": "Uruguay"
    },
    {
      "Nombre": "MuUruguay-1",
      "Deporte": "Basquet",
      "Genero": 0,
      "Nacionalidad": "Uruguay"
    },
    {
      "Nombre": "MuUruguay-1",
      "Deporte": "Futbol",
      "Genero": 0,
      "Nacionalidad": "Uruguay"
    },
    {
      "Nombre": "MuUruguay-1",
      "Deporte": "Basquet",
      "Genero": 0,
      "Nacionalidad": "Uruguay"
    },
    {
      "Nombre": "MuUruguay-1",
      "Deporte": "Futbol",
      "Genero": 0,
      "Nacionalidad": "Uruguay"
    },
    {
      "Nombre": "MuUruguay-1",
      "Deporte": "Tenis",
      "Genero": 0,
      "Nacionalidad": "Uruguay"
    },
    {
      "Nombre": "MujerUruguay-1",
      "Deporte": "Futbol",
      "Genero": 0,
      "Nacionalidad": "Uruguay"
    }
  ],
  "Delegaciones": [],
  "grupales": [],
  "restos": []
}