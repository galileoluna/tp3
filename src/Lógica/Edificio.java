package L�gica;

import java.util.ArrayList;

public class Edificio {
	public ArrayList<Departamento> edi;
	
	public Edificio() {
		this.edi=new ArrayList<Departamento>();
	}

	public ArrayList<Departamento> getEdi() {
		return edi;
	}
	
	public void setEdi(ArrayList<Departamento> edi) {
		this.edi = edi;
	}
	
	public void agregarDpto(Departamento dpto) {
		edi.add(dpto);
	}
	
	public boolean vacio(){
		if(edi.size()==0){
			return true;
		}
		return false;
	}
	
	public void imprimirDptos(){
		for(Departamento dep: edi){
			System.out.println(dep.toString());
		}
	}
	
	public Departamento dameDpto(int indice) {
		Departamento dpto=null;
		if(indice>edi.size()-1)
			throw new IllegalArgumentException("Departamento Inexistente");
		for(int i=0;i<edi.size();i++) {
			
			if(i==indice) {
				dpto= edi.get(i);
			}
		}
		return dpto;
	}
}