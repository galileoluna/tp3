package L�gica;

public class Atleta {
	private String Nombre;
	private String Deporte;
	private int Genero;
	private String Nacionalidad;
	
	public Atleta(String Nombre, String Deporte ,  String nacionalidad,int Genero){
		this.Nombre=Nombre;
		this.Deporte=Deporte;
		this.Genero=Genero;
		this.Nacionalidad=nacionalidad;
	}

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	public String getDeporte() {
		return Deporte;
	}

	public void setDeporte(String deporte) {
		Deporte = deporte;
	}

	public int getGenero() {
		return Genero;
	}

	public void setGenero(int genero) {
		Genero = genero;
	}

	public String getNacionalidad() {
		return Nacionalidad;
	}

	public void setNacionalidad(String nacionalidad) {
		Nacionalidad = nacionalidad;
	}
}