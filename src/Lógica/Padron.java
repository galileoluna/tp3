package L�gica;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Padron {

	public ArrayList<Atleta> mujeres;
	public ArrayList<Atleta> hombres;
	public ArrayList<Atleta> padron;
	public ArrayList<Delegacion> Delegaciones;
	public ArrayList<Atleta> grupales;
	public ArrayList<Atleta> restos;

	public Padron(ArrayList<Atleta> padron1) {
		this.padron = padron1;
		this.hombres = new ArrayList<Atleta>();
		this.mujeres = new ArrayList<Atleta>();
		this.Delegaciones = new ArrayList<Delegacion>();
		this.grupales = new ArrayList<Atleta>();
		this.restos = new ArrayList<Atleta>();
	}

	public ArrayList<Atleta> getMujeres() {
		return mujeres;
	}

	public void setMujeres(ArrayList<Atleta> mujeres) {
		this.mujeres = mujeres;
	}

	public ArrayList<Atleta> getHombres() {
		return hombres;
	}

	public void setHombres(ArrayList<Atleta> hombres) {
		this.hombres = hombres;
	}

	public ArrayList<Atleta> getPadron() {
		return padron;
	}

	public void setPadron(ArrayList<Atleta> padron) {
		this.padron = padron;
	}

	public ArrayList<Delegacion> getDelegaciones() {
		return Delegaciones;
	}

	public void setDelegaciones(ArrayList<Delegacion> delegaciones) {
		Delegaciones = delegaciones;
	}

	public void agregarDelegacion(Delegacion del) {
		Delegaciones.add(del);
	}

	public void agregarDeportistaDelegacion(Atleta dep) {
		for (int i = 0; i < Delegaciones.size(); i++) {
			String nacionalidad = dep.getNacionalidad();
			int genero = dep.getGenero();
			if (Delegaciones.get(i).getNombre().equals(nacionalidad)
					&& Delegaciones.get(i).getGenero() == genero) {
				Delegaciones.get(i).agregar(dep);
			}
		}
	}

	public boolean existeDelegacion(String pais, int genero) {
		for (int i = 0; i < Delegaciones.size(); i++) {
			if (Delegaciones.get(i).getNombre().equalsIgnoreCase(pais)
					&& Delegaciones.get(i).getGenero() == genero) {
				return true;
			}
		}
		return false;
	}

	public void dividir() {
		for (Atleta at : padron) {
			if (at.getGenero() == 0) {
				mujeres.add(at);
			} else {
				hombres.add(at);
			}
		}
	}

	public void grupalYResto() {
		for (Delegacion del : Delegaciones) {
			grupales.addAll(del.getGrupal());
			restos.addAll(del.getResto());
		}
	}

	public void agregarAtleta(Atleta nAtleta) {
		padron.add(nAtleta);
	}
	
	public void imprimirPadron(){
		for(Atleta at:hombres){
			System.out.println(at.getNacionalidad());
		}
		for(Atleta at:mujeres){
			System.out.println(at.getNacionalidad());
		}
	}
	
	public void imprimirDelegaciones() {
		for (Delegacion del : Delegaciones) {
			del.imprimirDelegacion();
		}
	}

	public void guardar(String archivo) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(this);
		try {
			FileWriter writer = new FileWriter(archivo);
			writer.write(json);
			writer.close();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.toString());
		}
	}

	public static Padron cargarArchivoGuardado(String archivo) {
		Gson gson = new Gson();
		Padron ret = null;
		try {
			BufferedReader br = new BufferedReader(new FileReader(archivo));
			ret = gson.fromJson(br, Padron.class);
		} catch (FileNotFoundException e) {
			JOptionPane
					.showMessageDialog(null, "error no encuentra el archivo");
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.toString());
		}
		return ret;
	}
}