package L�gica;

import java.util.ArrayList;

public class Departamento {
	ArrayList<Atleta> atletas;
	int genero;
	
	public Departamento(int sexo) {
		atletas=new ArrayList<Atleta>();
		genero=sexo;		
	}

	public Departamento() {
		atletas=new ArrayList<Atleta>();
		genero=0;
	}
	
	public void agregarAtleta(Atleta a){
		if(a.getGenero()!=genero)
			throw new IllegalArgumentException("Genero Invalido");
		if(atletas.size()>=4)
			throw new IllegalArgumentException("Departamento lleno, no se pudo agregar el atleta");
		if(atletas.size()<4){
			atletas.add(a);
		}
	}
	
	public boolean estaLleno() {
		if(atletas.size()<4){
			return false;
		}
		return true;
	}
	
	public String generoDepartamento() {
		if(genero==1) {
			return "Masculino";
		}
		if(genero==0) {
			return "Femenino";
		}
		return null;	
	}
	
	public String toString(){
		String ret="";
		for(int i=0;i<atletas.size();i++){
			ret+="["+atletas.get(i).getNombre()+" "+atletas.get(i).getNacionalidad()
					+" "+atletas.get(i).getDeporte()+"]\n";
		}
		return ret;
	}

	public int getGenero() {
		return genero;
	}

	public void setGenero(int genero) {
		this.genero = genero;
	}
	
	public ArrayList<Atleta> getAtletas() {
		return atletas;
	}

	public void setAtletas(ArrayList<Atleta> atletas) {
		this.atletas = atletas;
	}	
}