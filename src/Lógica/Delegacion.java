package L�gica;

import java.util.*;

public class Delegacion {
	ArrayList<Atleta> delegacion;
	String nombre;
	int genero;
	ArrayList<String> deportes;
	ArrayList<Integer> cantidad;
	ArrayList<Atleta> grupal;
	ArrayList<Atleta> resto;
	String mayordep;
	
	public Delegacion(String pais,int genero) {
		this.delegacion=new ArrayList<Atleta>();
		this.nombre=pais;
		this.genero=genero;
		this.deportes=new ArrayList<String>();
		this.cantidad=new ArrayList<Integer>();
		this.grupal=new ArrayList<Atleta>();
		this.resto=new ArrayList<Atleta>();
		this.mayordep="";		
	}

	public void dividir(){
		if(delegacion.size()%4==0){
			grupal.addAll(delegacion);
		}
		else{
			int rest=delegacion.size()%4;
			for(int i=0;i<delegacion.size();i++){
				if(i<delegacion.size()-rest){
					grupal.add(delegacion.get(i));
				}
				else{
					resto.add(delegacion.get(i));
				}
			}
		}
	}
	
	public void dividirPorDeporte(){
		Collections.sort(delegacion, new Comparator<Atleta>() {
			@Override
			public int compare(Atleta at1, Atleta at2){
				if(at1.getDeporte().equals(at2.getDeporte())){
					return 0;
				}
				else if(at1.getDeporte().equals(mayordep)){
					return -1;
				}
				else{
					return 1;
				}
			}	
	});
	}
	
	public void crearListaDeportes(){		
		for(int i=0;i<delegacion.size();i++){
			if(!existeDeporte(delegacion.get(i).getDeporte()))
				deportes.add(delegacion.get(i).getDeporte());
				cantidad.add(0);
			}		
	}
	
	public void cantidadDeporte(){
		for(String dep : deportes){
			for(Atleta at :delegacion){
				if(dep.equals(at.getDeporte())){
					int indice=deportes.indexOf(dep);
					cantidad.set(deportes.indexOf(dep), cantidad.get(indice)+1);
				}
			}
		}
	}
	
	int indiceDelDeporte(String deporte){
		for(int i=0;i<deportes.size();i++){
			if(deportes.get(i).equals(deporte)){
				return i;
			}
		}
		return -1;
	}	
	
	boolean existeDeporte(String dep){
		for(int i=0;i<deportes.size();i++){
			if(dep.equals(deportes.get(i))){
				return true;
			}
		}
		return false;
	}
	
	public boolean hayOtro(Atleta at){
		for(int i=0;i<delegacion.size();i++){
			if(!at.equals(delegacion.get(i)) && 
					at.getDeporte().equals(delegacion.get(i))){
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		String gene;
		if(genero==0){
			gene="Femenina";
		}
		else{
			gene="Masculina";
		}
		String ret="Delegacion "+nombre+" "+gene+ "\n";
		for(int i=0;i<delegacion.size();i++){
			ret+=delegacion.get(i).getNombre()+" "+delegacion.get(i).getDeporte()+"\n";
		}
		return ret;
	}

	public String imprimirListaDeDeportes(){
		String dep="";		
		for (int i=0;i<deportes.size();i++){
			dep+=deportes.get(i)+" "+cantidad.get(i)+"\n";
		}
		return dep;
	}

	public ArrayList<Atleta> getDelegacion() {
		return delegacion;
	}

	public void setDelegacion(ArrayList<Atleta> delegacion) {
		this.delegacion = delegacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getGenero() {
		return genero;
	}

	public void setGenero(int genero) {
		this.genero = genero;
	}

	public void imprimirDelegacion(){
		String ret="";
		for(Atleta at:delegacion){
			ret+=at.getNombre()+" "+at.getDeporte()+" "+at.getNacionalidad()+"\n";
		}
		System.out.println(ret);
	}
	
	public void imprimirTabla(){
		String ret=nombre+"\n";
		ret+=mayordep;
		System.out.println(ret);
	}

	public void agregar(Atleta dep) {
		delegacion.add(dep);		
	}
	
	int mayorIndice(){
		Integer max=0;
		for(Integer i:cantidad){
			if(i>max){
				max=i;
			}
		}
		return cantidad.indexOf(max);
	}
	
	void mayorDeporte(){
		mayordep=deportes.get(mayorIndice());
	}
	 
	public String dameMayor(){
		return mayordep;
	}

	public ArrayList<Atleta> getGrupal() {
		return grupal;
	}

	public void setGrupal(ArrayList<Atleta> grupal) {
		this.grupal = grupal;
	}

	public ArrayList<Atleta> getResto() {
		return resto;
	}

	public void setResto(ArrayList<Atleta> resto) {
		this.resto = resto;
	}	
}