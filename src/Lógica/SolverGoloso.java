package L�gica;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class SolverGoloso {
	private Padron listaDeportistas;
	private Edificio edificio;
	private ArrayList<Atleta> grupalMas;
	private ArrayList<Atleta> grupalFem;
	private ArrayList<Atleta> restoMas;
	private ArrayList<Atleta> restoFem;
	
	public SolverGoloso(Padron desordenado) {
		this.edificio = new Edificio();
		this.listaDeportistas = desordenado;
		this.grupalMas=new ArrayList<Atleta>();
		this.grupalFem=new ArrayList<Atleta>();
		this.restoMas=new ArrayList<Atleta>();
		this.restoFem=new ArrayList<Atleta>();
	}

	public void ordenar() {
		listaDeportistas.dividir();
		Collections.sort(listaDeportistas.getHombres(), new Comparator<Atleta>() {
			@Override
			public int compare(Atleta deportista1, Atleta deportista2) {
				return deportista1.getNacionalidad().compareTo(deportista2.getNacionalidad());
			}
		});
		Collections.sort(listaDeportistas.getMujeres(), new Comparator<Atleta>() {
			@Override
			public int compare(Atleta deportista1, Atleta deportista2) {
				return deportista1.getNacionalidad().compareTo(deportista2.getNacionalidad());
			}
		});	
	}
	
	public void designarDelegaciones() {
		ordenar();		
		//Ciclo masculinos
		for(Atleta at: listaDeportistas.getHombres()){
			if(listaDeportistas.existeDelegacion(at.getNacionalidad(),at.getGenero())){
				listaDeportistas.agregarDeportistaDelegacion(at);
			}
			else{
				listaDeportistas.agregarDelegacion(new Delegacion(at.getNacionalidad(),at.getGenero()));
				listaDeportistas.agregarDeportistaDelegacion(at);
			}
		}
		for(Atleta at: listaDeportistas.getMujeres()){
			if(listaDeportistas.existeDelegacion(at.getNacionalidad(),at.getGenero())){
				listaDeportistas.agregarDeportistaDelegacion(at);
			}
			else{
				listaDeportistas.agregarDelegacion(new Delegacion(at.getNacionalidad(),at.getGenero()));
				listaDeportistas.agregarDeportistaDelegacion(at);
			}
		}		
	}
	
	public void ordenarDelegaciones(){
		designarDelegaciones();		
		for(Delegacion del:listaDeportistas.getDelegaciones()){			
			del.crearListaDeportes();
			del.cantidadDeporte();
			del.mayorDeporte();
			del.dividirPorDeporte();
			del.dividir();
			if(del.getGenero()==1){
				grupalMas.addAll(del.getGrupal());
				restoMas.addAll(del.getResto());
			}
			else{
				grupalFem.addAll(del.getGrupal());
				restoFem.addAll(del.getResto());
			}
		}	
	}
		
	public ArrayList<Atleta> getGrupalMas() {
		return grupalMas;
	}

	public void setGrupalMas(ArrayList<Atleta> grupalMas) {
		this.grupalMas = grupalMas;
	}

	public ArrayList<Atleta> getGrupalFem() {
		return grupalFem;
	}

	public void setGrupalFem(ArrayList<Atleta> grupalFem) {
		this.grupalFem = grupalFem;
	}

	public ArrayList<Atleta> getRestoMas() {
		return restoMas;
	}

	public void setRestoMas(ArrayList<Atleta> restoMas) {
		this.restoMas = restoMas;
	}

	public ArrayList<Atleta> getRestoFem() {
		return restoFem;
	}

	public void setRestoFem(ArrayList<Atleta> restoFem) {
		this.restoFem = restoFem;
	}

	public void asignarHabitacion() {
		ordenarDelegaciones();
		Departamento nuevo=new Departamento(1);
		int indice=1;
		for(Atleta at:grupalMas){
			nuevo.agregarAtleta(at);
			if(indice%4==0){
				edificio.agregarDpto(nuevo);
				nuevo=new Departamento(1);
			}
			indice++;
		}
		indice=1;
		nuevo.setGenero(0);
		for(Atleta at:grupalFem){
			nuevo.agregarAtleta(at);
			if(indice%4==0){
				edificio.agregarDpto(nuevo);
				nuevo=new Departamento(0);
			}
			indice++;
		}
		indice=1;
		nuevo.setGenero(1);
		for(Atleta at:restoMas){
			nuevo.agregarAtleta(at);
			if(indice%4==0){
				edificio.agregarDpto(nuevo);
				nuevo=new Departamento(1);
			}
			else if(indice==restoMas.size()){
				edificio.agregarDpto(nuevo);
				nuevo=new Departamento(1);
			}
			indice++;
		}
		indice=1;
		nuevo.setGenero(0);
		for(Atleta at:restoFem){
			nuevo.agregarAtleta(at);
			if(indice%4==0){
				edificio.agregarDpto(nuevo);
				nuevo=new Departamento(0);
			}
			else if(indice==restoFem.size()){
				edificio.agregarDpto(nuevo);
				nuevo=new Departamento(0);
			}
			indice++;
		}		
	}

	public Padron getListaDeportistas() {
		return listaDeportistas;
	}

	public void setListaDeportistas(Padron listaDeportistas) {
		this.listaDeportistas = listaDeportistas;
	}

	public Edificio getEdificio() {
		return edificio;
	}

	public void setEdificio(Edificio edificio) {
		this.edificio = edificio;
	}	
}