package Test;

import static org.junit.Assert.*;
import java.util.ArrayList;
import L�gica.Atleta;
import L�gica.Edificio;
import L�gica.Departamento;
import L�gica.Padron;
import L�gica.SolverGoloso;
import org.junit.Test;

public class SolverGolosoTest {
	
	public SolverGoloso sgEjemplo(){
		SolverGoloso sg = new SolverGoloso (padronEjemplo());
		return sg;
	}

	public Padron padronEjemplo() {
		Padron padron= new Padron(new ArrayList <Atleta> ());		
		padron.agregarAtleta(new Atleta("Cambiazo","Argentina","Futbol",1));
		padron.agregarAtleta(new Atleta("Cavenaggi","Argentina","Futbol",1));
		padron.agregarAtleta(new Atleta("Mascherano","Argentina","Futbol",1));
		padron.agregarAtleta(new Atleta("Maradona","Argentina","Futbol",1));
		
		padron.agregarAtleta(new Atleta("Ale","Brasil","Futbol",1));
		padron.agregarAtleta(new Atleta("Kaka","Brasil","Futbol",1));
		padron.agregarAtleta(new Atleta("Pele","Brasil","Futbol",1));
		padron.agregarAtleta(new Atleta("Ronaldi�o","Brasil","Futbol",1));		
		return padron;
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void departamentoInexistenteTest() {
		Edificio Edi = new Edificio ();
		Edi.agregarDpto(new Departamento(0));
		Edi.dameDpto(1);
	}

	@Test 
	public void departamentosEsperados() {
		SolverGoloso sg = sgEjemplo();
		sg.asignarHabitacion();
		assertTrue(sg.getEdificio().getEdi().size() == 2); //cantidad de habitaciones esperada
	}	 
}