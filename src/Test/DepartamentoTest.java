package Test;

import L�gica.Atleta;
import L�gica.Departamento;
import org.junit.Test;

public class DepartamentoTest {

	public Atleta atletaMasculinoEjemplo() {		
		Atleta atleta = new Atleta("Martin Palermo","Argentina","Futbol",1);
		return atleta;
	}	  
	
	@Test (expected = IllegalArgumentException.class) 
	public void lugarDisponibleTest() {
		Departamento depto = new Departamento(1);
		depto.agregarAtleta(new Atleta("Pedro","Chile","Remo",1));
		depto.agregarAtleta(new Atleta("Diego","Argentina","Futbol",1));
		depto.agregarAtleta(new Atleta("Roberto","Brasil","Boxeo",1));
		depto.agregarAtleta(new Atleta("Juan","Bolivia","Taewkondo",1));
		depto.agregarAtleta(atletaMasculinoEjemplo());	
	}
	
	@Test (expected = IllegalArgumentException.class)	
	public void generoDistintoTest() {
		Departamento depto = new Departamento(0);
		depto.agregarAtleta(atletaMasculinoEjemplo());
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void agregarAtletaRepetidoTest() {
		Departamento depto = new Departamento(0);
		depto.agregarAtleta(atletaMasculinoEjemplo());
		depto.agregarAtleta(atletaMasculinoEjemplo());
	}
}